"use strict";/*!© Lorenzo L. Ancora, SPDX-License-Identifier: EUPL-1.2 !*/function barnightmode(mutationList,observer){let barnightmode=window.localStorage.getItem('bar_night_mode');let navbar=$(".navbar");if(barnightmode=='true'&&navbar.length>0){navbar.removeClass("bg-light navbar-light");navbar.addClass("bg-dark navbar-dark");}
else{navbar.removeClass("bg-dark navbar-dark");navbar.addClass("bg-light navbar-light");}
let navbtn=$(".navbar button[type='submit']");if(barnightmode=='true'&&navbtn.length>0){navbtn.addClass("btn-outline-light");}
else{navbtn.removeClass("btn-outline-light");}
if(observer!=null)
observer.disconnect();}
function mainnightmode(mutationList,observer){let main=$("main");if(main.length<=0)
return;let mainnightmode=window.localStorage.getItem('main_night_mode');main=null;let body=$("body");let dropdowns=$(".dropdown-menu");if(mainnightmode=='true'){body.removeClass("bg-body");body.addClass("bg-light bg-gradient");if(dropdowns.length>0){dropdowns.addClass("dropdown-menu-dark");if(observer!=null)
observer.disconnect();}}
else{body.removeClass("bg-light bg-gradient");body.addClass("bg-body");if(dropdowns.length>0){dropdowns.removeClass("dropdown-menu-dark");if(observer!=null)
observer.disconnect();}}}
function mgrfonts(mutationList,observer){let body=$("body");if(body.length<=0)
return;else if(observer!=null)
observer.disconnect();let spacedfonts=window.localStorage.getItem('fonts_spaced');let ssfonts=window.localStorage.getItem('fonts_sans_serif');if(spacedfonts=='true'){body.css('letter-spacing','0.02rem');}
else{body.css('letter-spacing','initial');}
if(ssfonts=='true'){body.css('font-family','sans-serif');}
else{body.css('font-family','var(--bs-font-sans-serif)');}}
function mgrbt(p1,p2){let html=$("html");let slowmo=window.localStorage.getItem('animations_slow');let h=$("html");if(slowmo=='true'){$['animation_speed']="slow";h.addClass("slow");}
else{$['animation_speed']="fast";h.removeClass("slow");if(h.attr("class")=="")
h.removeAttr("class");}}
function mgrrtt(){let pageh=$(document).height();let viewporth=$(window).height();if(pageh==undefined||viewporth===undefined)
return;function returntotop(){window.scrollTo(0,0);}
if(pageh>viewporth){let rtt=$("footer #rtt");rtt.on("click",returntotop);rtt.removeClass("visually-hidden");}}
{const allHTML=document.querySelector("html");const barObserver=new MutationObserver(barnightmode);const mainObserver=new MutationObserver(mainnightmode);const fontObserver=new MutationObserver(mgrfonts);const observerOptions={childList:true,attributes:false,subtree:false};if(allHTML!=null){barObserver.observe(allHTML,observerOptions);mainObserver.observe(allHTML,observerOptions);fontObserver.observe(allHTML,observerOptions);}}
$(mgrbt);$(mgrrtt);