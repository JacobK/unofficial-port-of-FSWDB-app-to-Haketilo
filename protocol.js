"use strict";/*!© Lorenzo L. Ancora, SPDX-License-Identifier: EUPL-1.2 !*/var HypertextTransfer;(function(HypertextTransfer){function issecure(){return window.location.protocol.startsWith("https");}
function istorified(){return window.location.hostname.endsWith(".onion")||window.location.hostname.endsWith(".onion.");}
HypertextTransfer.istorified=istorified;function islocal(){return window.location.hostname.indexOf("127")>=0;}
function autoredirect(){if(islocal())
return false;if(issecure()==false&&istorified()==false){window.location.protocol="https:";}
return true;}
HypertextTransfer.autoredirect=autoredirect;})(HypertextTransfer||(HypertextTransfer={}));var URIs;(function(URIs){function reverturlbar(e){window.setTimeout(function(){try{history.pushState("",document.title,window.location.pathname+window.location.search);}
catch(err){}},150);}
function defusehashuris(){let hashlinks=$("a[href='#']");hashlinks.on('click',reverturlbar);}
URIs.defusehashuris=defusehashuris;})(URIs||(URIs={}));$(function(){HypertextTransfer.autoredirect();URIs.defusehashuris();});