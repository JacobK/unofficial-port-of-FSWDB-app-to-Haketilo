# SPDX-FileCopyrightText: 2022 Jacob K
#
# SPDX-License-Identifier: CC0-1.0

# general
wget "https://polyfill.io/v3/polyfill.min.js?version=3.111.0&features=es2022%2Ces2021%2Ces2020%2Ces2019%2Ces2018%2Ces2017"
wget "https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"
wget "https://fswdb.eu/static/js/protocol.js"
wget "https://fswdb.eu/static/js/accessibility.js"
wget "https://fswdb.eu/static/js/navbar.js"
wget "https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"

# home (https://fswdb.eu/)
wget "https://fswdb.eu/static/js/home.js"

# search (https://fswdb.eu/search?s=gnu)
wget "https://fswdb.eu/static/js/serp.js"

# accessibility options (https://fswdb.eu/options/accessibility)
wget "https://fswdb.eu/static/js/accessibility/Inputs.js"

# tokens options (https://fswdb.eu/options/tokens)
wget "https://fswdb.eu/static/js/tokens/settings.js"
wget "https://cdn.jsdelivr.net/npm/crypto-js@4/crypto-js.min.js"
wget "https://fswdb.eu/static/js/tokens/Security.js"

# about
wget "https://fswdb.eu/static/js/about.js"
# wget "https://fswdb.eu/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js" # removed because it was nonfree
