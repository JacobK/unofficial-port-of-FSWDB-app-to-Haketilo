"use strict";/*!© Lorenzo L. Ancora, SPDX-License-Identifier: EUPL-1.2 !*/var Page;(function(Page){function scroll(e){window.scrollTo(e.data.X,e.data.Y);}
Page.scroll=scroll;function mgrkarmaslider(e){let slider=$(e.target);let label=e.data.label;let sval=Number(slider.val());let percLikes=((sval+5)/10)*100;if(sval<-1)
label.text(sval+" ("+percLikes+"%): bad.");else if(sval>1)
label.text(sval+" ("+percLikes+"%): good.");else
label.text(sval+" ("+percLikes+"%): neutral.");}
Page.mgrkarmaslider=mgrkarmaslider;})(Page||(Page={}));var Tor;(function(Tor){function copylink(e){let val=String($(e.target).val()).toString();let clipboard=navigator.clipboard;if(clipboard!==undefined){clipboard.writeText(val);}}
Tor.copylink=copylink;})(Tor||(Tor={}));$(function(){$("button[data-bs-toggle=modal]").on("click",{x:0,y:0},Page.scroll);$("a.torlink").on("click",Tor.copylink);$("#dq").on("change",{"label":$("label[for=dq]")},Page.mgrkarmaslider);});