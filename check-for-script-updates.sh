# SPDX-FileCopyrightText: 2022 Jacob K
#
# SPDX-License-Identifier: CC0-1.0

mkdir tempdir-verify
cd tempdir-verify
../download_official_scripts.sh
cp ../sha256sums_official_2022-10-07.txt .
sha256sum -c sha256sums_official_2022-10-07.txt
cd ..
rm -r tempdir-verify
